const ATTACK_VALUE = 10;
const MONSTER_ATTACK_VALUE = 14;

let totalPlayerAttacks = 0;
let totalMonsterAttacks = 0;

const STRONG_ATTACK_VALUE = 20;
const STRONG_MONSTER_ATTACK_VALUE = 30;

const MODE_ATTACK = 'ATTACK';
const MODE_STRONG_ATTACK = 'STRONG ATTACK';

const LOG_EVENT_PLAYER_ATTACK = 'PLAYER_ATTACK';
const LOG_EVENT_PLAYER_STRONG_ATTACK = 'PLAYER_STRONG_ATTACK';
const LOG_EVENT_MONSTER_ATTACK = 'MONSTER_ATTACK';
const LOG_EVENT_PLAYER_HEAL = 'PLAYER_HEAL';
const LOG_EVENT_GAME_OVER = 'GAME_OVER';

let battleLog = [];
let lastEventRegistered;

const HEAL_VALUE = 20;

let valuePlayerMaxLife = 0;
let chosenMaxLife = 100;

// eslint-disable-next-line no-undef
inputHealtValue.addEventListener('change', getInputValueHandler);

let currentMonsterHealth = chosenMaxLife;
let currentPlayerHealth = chosenMaxLife;

let hasBonusLife = true;

// eslint-disable-next-line no-undef
adjustHealthBars(chosenMaxLife);

function writeLogEvent(
	event,
	value,
	currentMonsterHealth,
	currentPlayerHealth,
	totalMonsterAttacks,
	totalPlayerAttacks
) {
	let logEntryEvent = {
		event: event,
		value: value,
		finalMonsterHealth: currentMonsterHealth,
		finalyPlayerHealth: currentPlayerHealth,
		totalPlayerAttacks: totalPlayerAttacks,
		totalMonsterAttacks: totalMonsterAttacks,
	};
	if (event === LOG_EVENT_PLAYER_ATTACK) {
		logEntryEvent.target = 'MONSTER';
	} else if (event === LOG_EVENT_PLAYER_STRONG_ATTACK) {
		logEntryEvent.target = 'MONSTER';
	} else if (event === LOG_EVENT_MONSTER_ATTACK) {
		logEntryEvent.target = 'PLAYER';
	} else if (event === LOG_EVENT_PLAYER_HEAL) {
		logEntryEvent.target = 'PLAYER';
	} else if (event === LOG_EVENT_GAME_OVER) {
		logEntryEvent.totalPlayerAttacks = totalPlayerAttacks;
		logEntryEvent.totalMonsterAttacks = totalMonsterAttacks;
	}
	battleLog.push(logEntryEvent);
}

function getInputValueHandler() {
	// eslint-disable-next-line no-undef
	valuePlayerMaxLife = inputHealtValue.value;

	chosenMaxLife = parseInt(valuePlayerMaxLife);
	if (isNaN(chosenMaxLife) || chosenMaxLife <= 0) {
		// chosenMaxLife = 100;
		// console.log('valuePlayerMaxLife');
		throw { message: 'Invalid input value' };
	}
}

function reset() {
	currentMonsterHealth = chosenMaxLife;
	currentPlayerHealth = chosenMaxLife;
	resetGame(chosenMaxLife);
}
function endGame() {
	let initialPlayerHealth = currentPlayerHealth;
	const playerDamage = dealPlayerDamage(MONSTER_ATTACK_VALUE);
	currentPlayerHealth -= playerDamage;
	totalMonsterAttacks++;
	writeLogEvent(
		LOG_EVENT_PLAYER_ATTACK,
		playerDamage,
		currentMonsterHealth,
		currentPlayerHealth,
		totalMonsterAttacks,
		totalPlayerAttacks
	);

	if (currentPlayerHealth <= 0 && hasBonusLife) {
		hasBonusLife = false;
		removeBonusLife();
		currentPlayerHealth = initialPlayerHealth;
		setPlayerHealth(initialPlayerHealth);
		alert('you lost a life');
	}
	if (currentMonsterHealth <= 0 && currentPlayerHealth > 0) {
		alert('you win');
		writeLogEvent(
			LOG_EVENT_GAME_OVER,
			'you win',
			currentPlayerHealth,
			currentMonsterHealth,
			totalMonsterAttacks,
			totalPlayerAttacks
		);
	} else if (currentPlayerHealth <= 0 && currentMonsterHealth > 0) {
		alert('player win');
		writeLogEvent(
			LOG_EVENT_GAME_OVER,
			'monster win',
			currentPlayerHealth,
			currentMonsterHealth,
			totalMonsterAttacks,
			totalPlayerAttacks
		);
	} else if (currentMonsterHealth <= 0 && currentPlayerHealth <= 0) {
		alert('you are a draw');
		writeLogEvent(
			LOG_EVENT_GAME_OVER,
			'you are a draw',
			currentPlayerHealth,
			currentMonsterHealth,
			totalMonsterAttacks,
			totalPlayerAttacks
		);
	}
	if (currentMonsterHealth <= 0 || currentPlayerHealth <= 0) {
		reset();
	}
}
function attackMonster(mode) {
	let maxDamage = mode === MODE_ATTACK ? ATTACK_VALUE : STRONG_ATTACK_VALUE;
	let eventAttack =
		mode === MODE_ATTACK
			? LOG_EVENT_PLAYER_ATTACK
			: LOG_EVENT_PLAYER_STRONG_ATTACK;
	// if (mode === MODE_ATTACK) {
	// 	maxDamage = ATTACK_VALUE;
	// 	eventAttack = LOG_EVENT_PLAYER_ATTACK;

	// 	totalPlayerAttacks++;
	// } else if (mode === MODE_STRONG_ATTACK) {
	// 	maxDamage = STRONG_ATTACK_VALUE;
	// 	eventAttack = LOG_EVENT_PLAYER_STRONG_ATTACK;
	// 	totalPlayerAttacks++;
	// } else {
	// 	maxDamage = MONSTER_ATTACK_VALUE;
	// }

	const damage = dealMonsterDamage(maxDamage);
	currentMonsterHealth -= damage;
	writeLogEvent(
		eventAttack,
		damage,
		currentMonsterHealth,
		currentPlayerHealth,
		totalPlayerAttacks,
		totalMonsterAttacks
	);

	endGame();
}

function attackHandler() {
	attackMonster('ATTACK');
}

function strongAttackHandler() {
	attackMonster('STRONG ATTACK');
}
function healPlayerHandler() {
	let healValue;

	if (currentPlayerHealth >= chosenMaxLife - HEAL_VALUE) {
		alert("you can't heal more than your max initial health");
		healValue = chosenMaxLife - currentPlayerHealth;
	} else {
		healValue = HEAL_VALUE;
	}

	increasePlayerHealth(HEAL_VALUE);
	currentPlayerHealth += healValue;
	writeLogEvent(
		LOG_EVENT_PLAYER_HEAL,
		healValue,
		currentMonsterHealth,
		currentPlayerHealth,
		totalMonsterAttacks,
		totalPlayerAttacks
	);

	endGame();
}
function printLogHandler() {
	let i = 0;

	for (const logEntry of battleLog) {
		if (
			(!lastEventRegistered && lastEventRegistered !== 0) ||
			lastEventRegistered < i
		) {
			console.log(` i------>${i}`);
			for (const key in logEntry) {
				console.log(` key----> :${key}: ${logEntry[key]}`);
				console.log('----------hooolaaa');
			}
			lastEventRegistered = i;
			console.log('lastEventRegistered', lastEventRegistered);
		}
		i++;
		// break;
	}
}
attackBtn.addEventListener('click', attackHandler);
strongAttackBtn.addEventListener('click', strongAttackHandler);
healBtn.addEventListener('click', healPlayerHandler);
logBtn.addEventListener('click', printLogHandler);
